"""Module that keeps all services and clients templates.generate all"""

import os
import random
import string

DIFFERENT_DATA = {
	"naturalization_mariage": {
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/4485/0",
		"8": ["firstname", "lastname"]
	},

	"naturalization_decret": {
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/4462/0",
		"8": ["firstname", "lastname"]
	},

	"naturalization_fratrie": {
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/9087/0",
		"8" : ["firstname", "lastname"]
	},

	"titre_privee": {
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/8483/0",
		"8": ["firstname", "lastname", "number_agdref", "end_date_validity", "post_code"]
	},

	"titre_salarie":{
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/12491/0",
		"8": ["firstname", "lastname", "eZBookingAdditionalField_value_12960"]
	},

	"titre_etudiant": {
		"URL": "http://www.hauts-de-seine.gouv.fr/booking/create/8482/0",
		"8": ["firstname", "lastname", "number_agdref", "end_date_validity", "post_code"]
	},

	"bobigny_titre_salarie": {
		"URL": "http://www.seine-saint-denis.gouv.fr/booking/create/9829/0",
		"8": ["firstname", "lastname", "eZBookingAdditionalField_value_12960"]
	},

	"naturalization_decret_vdm": {
		"URL": "http://www.val-de-marne.gouv.fr/booking/create/4963/0",
		"1": {"planning": "5985"},
		"8": ["firstname", "lastname"]
	}
}

SERVICE_TEMPLATE = {
	"URL": None,
	"date_from": None,
	"links": {
		"en": {
			"0": {"nextButton": "Create a new booking", "condition": "on"},
			"1": {"nextButton": "Next step"},
			"2": {"nextButton": "Next step"},
			"3": {"nextButton": "Next step"},
			"4": {"nextButton": "First free timeslot"},
			"6": {"nextButton": "Next step", "eZHumanCAPTCHACode": None},
			"8": {"nextButton": "Next step"}
		},
		"fr": {
			"0": {"nextButton": "Effectuer+une+demande+de+rendez-vous", "condition": "on"},
			"1": {"nextButton": "Etape+suivante","planning": "7070"},
			"2": {"nextButton": "Etape+suivante"},
			"3": {"nextButton": "Etape+suivante"},
			"4": {"nextButton": "Première plage horaire libre"},
			"6": {"nextButton": "Etape+suivante","eZHumanCAPTCHACode": None},
			"8": {"nextButton": "Etape+suivante"}
		}
	}
}

URL_JS = 'https://rdv-etrangers-92.interieur.gouv.fr/eAppointmentpref92/element/jsp/specific/pref92.jsp'

SERVICE_CODES_JS = {
	"1": 2,
	"js_biometrie": 10,
	"js_sejour_etudiant": 1,
	"js_permis_conduire": 9,
	"2": 5,
	"js_sejour_adress": 3,
	"js_renouv_conjoint": 6,
	"js_renouv_parent": 7,
	"js_renouv_salarie": 8,
	"js_renouv_10ans": 4,
	"3": 12, #Service from two
	"4": 11,
	"5": 13  #Service from two
}

# 'civility' value 'MR'(monsieur) or 'MME'(madame)
SERVICE_TEMPLATE_JS = ['civility', 'lastname', 'firstname', 'phone', 'postcode']


def get_email(type='fixed'):
	# type = ['fixed', 'dot', 'plus']
	"""returns email from email_list or generates new email by adding 6 random symbols"""
	email_list = ['chat111306@gmail.com',
				  'sunny861034@gmail.com',
				  'soleil931242@gmail.com',
				  'rdv.prefecture.de.nanterre@gmail.com']
	email = random.choice(email_list)
	if type == 'fixed':
		return email

	elif type == 'dot':
		"""Email with dot in random position"""
		pos = random.randint(-len(email)+1,-11)
		return '{0}.{1}'.format(email[:pos], email[pos:])

	elif type == 'plus':
		"""Email generator with 6 additional symbols added"""
		length = 6
		chars = string.ascii_letters + string.digits + '!@#$%^&*()'
		random.seed = (os.urandom(1024))
		return ''.join([email[:-10], '+', ''.join(random.choice(chars) for i in range(length)), email[-10:]])


def generate_client_json(client):
	"""Generates requests matrix from SERVICE_TEMPLATE by filling client values from client_dict based on DIFFERENT_DATA"""

	SERVICE_TEMPLATE["URL"] = DIFFERENT_DATA[client["service"]]["URL"]
	SERVICE_TEMPLATE["date_from"] = client["date_from"]
	assigned_email = get_email(type='dot')
	print('email :', assigned_email)
	for lang, pages in SERVICE_TEMPLATE["links"].items():
		for key in DIFFERENT_DATA[client['service']]["8"]:
			try:
				pages["8"][key] = client["client_data"][key]
			except KeyError as e:
				print("[WARNING] For lang: {}, no value: {} in client data".format(lang, e))
		pages["8"]["email"] = assigned_email
		pages["8"]["emailcheck"] = assigned_email
	if "1" in DIFFERENT_DATA[client["service"]]:
		for lang, pages in SERVICE_TEMPLATE["links"].items():
			pages["1"]["planning"] = DIFFERENT_DATA[client["service"]]["1"]["planning"]
	# print(SERVICE_TEMPLATE)
	return SERVICE_TEMPLATE


def generate_js_client_data(client):
	""""""
	service_code = SERVICE_CODES_JS[client["service"]]
	data = dict()
	for field in SERVICE_TEMPLATE_JS:
		try:
			data[field] = client["client_data"][field]
		except KeyError as e:
			print("[WARNING] No value: {} in client data".format(e))
	date_from = client["date_from"]
	email = get_email(type='dot')
	data["email"] = email

	return {"service_code": service_code, "date_from": date_from, "data": data}