from PIL import Image
import glob
import os
import numpy as np
import psutil

import pytesseract

os.environ["TESSDATA_PREFIX"] = os.path.join("C:", "Program Files (x86)", "Tesseract-OCR", "tessdata")
pytesseract.pytesseract.tesseract_cmd = os.path.join("C:", "Program Files (x86)", "Tesseract-OCR", "tesseract.exe")
#print(os.environ.get('TESSDATA_PREFIX'))


def img_to_black_and_white(in_path, out_path, w=134):
    with Image.open(in_path) as img:
        for x in range(img.size[1]):
            for y in range(img.size[0]):
                pix = np.array(img.getpixel((y, x)))
                if sum(pix) > w*3:
                    img.putpixel((y, x), (255, 255, 255))
                else:
                    img.putpixel((y, x), (0, 0, 0))
    img.save(out_path)


def recognize_captcha_pytesseract(path, oem=2):
    """Now works only for number containing captchas"""
    with Image.open(path) as img:
        rec = pytesseract.image_to_string(img, config='--psm 8 --oem %s -c tessedit_char_whitelist=0123456789' % oem)
        rec = rec.replace(' ', '')
    return rec


def def_probability(path, method):
    '''Calculates capthcha recognition rate with method defined.

    path: captchas folder with filenames as sorrect captchas names
    '''

    t = 0
    f = 0
    for captcha in os.listdir(path):
        if (captcha[:4] != 'pic_') and (captcha[:4] != 'capt'):
            true = captcha[:-4]
            #print('captcha:', captcha)
            rec = method(os.path.join(path,captcha))
            if rec == true:
                t += 1
                print('True')
            else:
                print(true)
                print(rec)
                f += 1
                print('False')
                #input()
    print('true:', t)
    print('false', f)
    print('ratio', t/(t+f))
    return t/(t+f)


def safe_rename(file0, file1, add=0):
    split = file1.split(".")
    part1 = ".".join(split[:-1]) + "_" + str(add)
    file = ".".join([part1, split[-1]])
    if not os.path.isfile(file):
        #print(file)
        print('To save', file)
        os.rename(file0, file)
    else:
        add += 1
        safe_rename(file0, file1, add=add)


def manual_rename_captchas(path):
    '''Renaming captchas in a folder path by input'''
    captchas = os.listdir(path)
    for captcha in captchas[:]:
        if captcha[:3] == 'pic':
            with Image.open(os.path.join(path, captcha)) as img:
                img.show()
                new_name = input('Input captcha text :')
                # hide image
            for proc in psutil.process_iter():
                if proc.name() == "display":
                    proc.kill()
            print(os.path.join(path, captcha))
            print(os.path.join(path, new_name+'.jpg'))
            safe_rename(os.path.join(path, captcha), os.path.join(path, new_name+'.jpg'), add=0)
            #os.rename(os.path.join(path, captcha), os.path.join(path, new_name+'.jpg'))

manual_rename_captchas('to_recogn')
#def_probability('to_recogn', vision_recognize)

# for vision js_captchas
# true: 34
# false 74
# ratio 0.3148148148148148

