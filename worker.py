# Script to write client data and to begin registration
# For static and dynamic pages two modules are imported respectively
"""
Usage example:
python worker.py
"""

"""
ALL JS SERVICIES
https://rdv-etrangers-92.interieur.gouv.fr/eAppointmentpref92/element/jsp/appointment.jsp
Biométrie - prise d'empreintes digitales par suite d'une convocation préalable de la préfecture 
"""

# TODO to rellocate to digitalocean server
# TODO check opportunities to change ip or proxy settings on digitalocean
# TODO make module or function for choosing connection method from "static(localhost)", "proxy_list", "tor"

# TODO start date condition
# TODO test JS registration and cancelling.

import os
import argparse
import json

from registration_static import Registration
from registration_js import RegistrationJS
import data_new


def get_unique_client_list(source='./clients/', n=4):
    """Takes each client from clients folder and returns list of max n clients, one client for each service"""

    files = os.listdir(source)
    # Loading clients array
    all_clients = list()
    for f in files:
        with open(source + f) as json_data:
            client_data = json.load(json_data)
            all_clients.append(client_data)
    # Make set of services present
    services_set = set()
    for client in all_clients:
        services_set.add(client['service'])
    # Getting first n clients of different services
    output_list = list()
    for service in services_set:
        for client in all_clients:
            if client['service'] == service:
                output_list.append(client)
                break
        output_list.append()
    return output_list[:n]


def main(args):
    with open(args.client_file) as json_data:
        client_data = json.load(json_data)
    for k, v in client_data.items():
        print(k,':\t', v)
    if client_data['service'][:3] == 'js_':
        client_json = data_new.generate_js_client_data(client_data)
        session = RegistrationJS(client_json)
    else:
        client_json = data_new.generate_client_json(client_data)
        session = Registration(client_json)
    session.start_registration()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Registration worker")
    parser.add_argument("client_file", type=str, help="client json file")
    main(parser.parse_args())
